module.exports = function () {

	this.Given(/^I will go on the home page$/, function (callback) {
		this.client.url(process.env.ROOT_URL).call(callback);
	});

	this.Given(/^There is an event with guests$/, function (callback) {
		this.server.call('test/insertEvent', {name: "New Tested Event With Guests", date: new Date(), guests: [{
			name: "Guest 1",
			status: "attending"
		},{
			name: "Guest 2",
			status: "not attending"
		},{		name: "Guest 3",
			status: "maybe"
		}]}).then(callback);
	
	});

	this.When(/^I click on the event title$/, function (callback) {
		this.client
			.waitForExist('h4 a', 7000)
			.waitForVisible('h4 a', 2000)
			.click('h4 a')
			.call(callback);
	});

	this.Then(/^I see event page$/, function (callback) {
		this.client
			.waitForExist('article h3', 7000)
			.waitForVisible('article h3', 2000)
			.getText('article h3')
			.should.become('New Tested Event With Guests')
			.call(callback);
	});

	this.Then(/^I see list of guests separated with their status$/, function (callback) {
		this.client
			.elements('.guest__name', function(err, res){
				assert.equal(res.value.length, 3);
			})
			.elements('[data-status="attending"] .guest', function(err, res){
				assert.equal(res.value.length, 1);
			})
			.elements('[data-status="maybe"] .guest', function(err, res){
				assert.equal(res.value.length, 1);
			})
			.elements('[data-status="not attending"] .guest', function(err, res){
				assert.equal(res.value.length, 1);
			})
			.call(callback);
	});

};