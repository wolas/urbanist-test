Feature: Visit an event

  As a user
  I want to see an event
  So that I can manage the guests

  	@dev
	Scenario: User can see guests in the event
		Given I will go on the home page
		And There is an event with guests
		When I click on the event title
		Then I see event page
		And I see list of guests separated with their status