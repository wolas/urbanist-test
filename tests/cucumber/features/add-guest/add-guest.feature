Feature: Add a guest

  As a user
  I want to see an event
  So that I can add a guest

  	@dev
	Scenario: User can add a guest when providig all necessary fields
		Given I will go on the home page
		And There is an event without guests
		When I click on the event title
		Then I see event page
		When I click on the add guest button
		Then A modal with pop up
		When I provide all necessary fields in the form
		When click submit button
		Then the modal will close down
		And the guest will be visible in the event
