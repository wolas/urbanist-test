module.exports = function () {

	this.Given(/^I will go on the home page$/, function (callback) {
		this.client.url(process.env.ROOT_URL).call(callback);
	});

	this.Given(/^There is an event with guest$/, function (callback) {
		this.server.call('test/insertEvent', {name: "New Tested Event With Guests", date: new Date(), guests: [{
			name: "Guest 1",
			status: "attending"
		}]}).then(callback);
	
	});

	this.When(/^I click on the event title$/, function (callback) {
		this.client
			.waitForExist('h4 a', 7000)
			.waitForVisible('h4 a', 2000)
			.click('h4 a')
			.call(callback);
	});

	this.Then(/^I see event page$/, function (callback) {
		this.client
			.waitForExist('article h3', 7000)
			.waitForVisible('article h3', 2000)
			.getText('article h3')
			.should.become('New Tested Event With Guests')
			.call(callback);
	});

	this.Given(/^There is an event without guests$/, function (callback) {
 		this.server.call('test/insertEvent', {name: "New Tested Event With Guests", date: new Date(), guests: []}).then(callback);
	});

	this.When(/^I click on the add guest button$/, function (callback) {
		this.client
			.waitForExist('.event-page__guestsHeader .btn', 7000)
			.waitForVisible('.event-page__guestsHeader .btn', 2000)
			.click('.event-page__guestsHeader .btn')
			.call(callback);
	});

	this.Then(/^A modal with pop up$/, function (callback) {
		this.client
			.waitForExist('.modal-open', 7000)
			.waitForVisible('.modal-open', 2000)
			.waitForExist('form.addGuest-form', 7000)
			.waitForVisible('form.addGuest-form', 2000)
			.call(callback)
	});

	this.When(/^I provide all necessary fields in the form$/, function (callback) {
		this.client
			.waitForExist('input.guest-form__name', 2000)
			.waitForExist('input.guest-form__status', 2000)
			.setValue('input.guest-form__name', 'Super New Tested Guest')
			.call(callback)
	});

	this.When(/^click submit button$/, function (callback) {
		this.client
			.submitForm('form.addGuest-form')
			.call(callback);
	});

	this.Then(/^the modal will close down$/, function (callback) {
		this.client
			.pause(1000)
			.elements('.modal-open', function(err, res){
				assert.equal(res.value.length, 0);
			})
			.call(callback)
	});

	this.Then(/^the guest will be visible in the event$/, function (callback) {
		this.client
			.elements('.guest__name', function(err, res){
				assert.equal(res.value.length, 1);
			})
			.call(callback)
	});


};