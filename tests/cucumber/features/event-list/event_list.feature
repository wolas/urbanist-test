Feature: Show an event list

  As a user
  I want to see a event list
  So that I can preview events

  	@dev
	Scenario: There is one event 
		Given There is one event named "Hello World Party"
		When a user navigates to the home page
		Then users can see this event named "Hello World Party"

	@dev
	Scenario: There are two events 
		Given There are two events named "Event first" and "Event second"
		When a user navigates to the home page
		Then users can see those events