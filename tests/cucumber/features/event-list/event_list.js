module.exports = function () {

	this.Given(/^There is one event named "([^"]*)"$/, function (name, callback) {
		this.server.call('test/insertEvent', {name: name, date: new Date(), guests: []}).then(callback);
	});

	this.Given(/^There are two events named "([^"]*)" and "([^"]*)"$/, function (name1, name2, callback) {
		this.server.call('test/insertEvent', {name: name1, date: new Date(), guests: []});
		this.server.call('test/insertEvent', {name: name2, date: new Date(), guests: []}).then(callback);
	});

	this.When(/^a user navigates to the home page$/, function (callback) {
		this.client.url(process.env.ROOT_URL).call(callback);
	});

	this.Then(/^users can see this event named "([^"]*)"$/, function (name, callback) {
		this.client
			.waitForExist('h4', 7000)
			.waitForVisible('h4', 2000)
			.getText('h4 a')
			.should.become(name)
			.and.notify(callback);
	});

	this.Then(/^users can see those events$/, function (callback) {
		this.client
			.waitForExist('h4', 7000)
			.waitForVisible('h4', 2000)
			.elements('h4 a', function(err, res){
				assert.equal(res.value.length,2);
			})
			.call(callback);
	});
};