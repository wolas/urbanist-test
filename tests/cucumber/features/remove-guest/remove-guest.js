module.exports = function () {

	this.Given(/^I will go on the home page$/, function (callback) {
		this.client.url(process.env.ROOT_URL).call(callback);
	});

	this.Given(/^There is an event with guest$/, function (callback) {
		this.server.call('test/insertEvent', {name: "New Tested Event With Guests", date: new Date(), guests: [{
			name: "Guest 1",
			status: "attending"
		}]}).then(callback);
	
	});

	this.When(/^I click on the event title$/, function (callback) {
		this.client
			.waitForExist('h4 a', 7000)
			.waitForVisible('h4 a', 2000)
			.click('h4 a')
			.call(callback);
	});

	this.Then(/^I see event page$/, function (callback) {
		this.client
			.waitForExist('article h3', 7000)
			.waitForVisible('article h3', 2000)
			.getText('article h3')
			.should.become('New Tested Event With Guests')
			.call(callback);
	});

	this.When(/^I click on the remove button$/, function (callback) {
		this.client
			.waitForExist('.btn-remove', 7000)
			.waitForVisible('.btn-remove', 2000)
			.click('.btn-remove')
			.call(callback);
	});

	this.Then(/^the guest will be removed from the event$/, function (callback) {
		this.client
			.pause(1000)
			.elements('.guest__name', function(err, res){
				assert.equal(res.value.length, 0);
			})
			.call(callback);
	});

};