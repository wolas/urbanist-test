Feature: Remove a guest from the event

  As a user
  I want to visit the event
  So that I can remove a guest

  	@dev
	Scenario: User can remome a guest from the event
		Given I will go on the home page
		And There is an event with guest
		When I click on the event title
		Then I see event page
		When I click on the remove button
		Then the guest will be removed from the event
