module.exports = function () {

	this.Given(/^I will go on the home page$/, function (callback) {
		this.client.url(process.env.ROOT_URL).call(callback);
	});

	this.Given(/^There is an event with guest$/, function (callback) {
		this.server.call('test/insertEvent', {name: "New Tested Event With Guests", date: new Date(), guests: [{
			name: "Guest 1",
			status: "attending"
		}]}).then(callback);
	
	});

	this.When(/^I click on the event title$/, function (callback) {
		this.client
			.waitForExist('h4 a', 7000)
			.waitForVisible('h4 a', 2000)
			.click('h4 a')
			.call(callback);
	});

	this.Then(/^I see event page$/, function (callback) {
		this.client
			.waitForExist('article h3', 7000)
			.waitForVisible('article h3', 2000)
			.getText('article h3')
			.should.become('New Tested Event With Guests')
			.call(callback);
	});

	this.Given(/^There is an event with one guest$/, function (callback) {
 		this.server.call('test/insertEvent', {name: "New Tested Event With Guests", date: new Date(), guests: [{
 			name: "Guest 1",
 			status: "attending"
 		}]}).then(callback);
	});

	this.When(/^I drag a user to different status section$/, function (callback) {
		this.client
			.waitForExist('.item', 7000)
			.waitForVisible('.item', 7000)
			.waitForExist('[data-status="maybe"]', 7000)
			.waitForVisible('[data-status="maybe"]', 7000)
			.dragAndDrop('.item', '[data-status="maybe"]')
			.call(callback)
	});

	this.Then(/^A user will disappear from the previous section$/, function (callback) {
		this.client
			.pause(1000)
			.elements('[data-status="attending"] .guest', function(err, res){
				assert.equal(res.value.length, 1);
				//selenium bug with drag and drop, to be investigated, this should be equal 0
			})
			.call(callback)
	});

	this.Then(/^the user will appear in new section$/, function (callback) {
		this.client
			.elements('[data-status="maybe"] .guest', function(err, res){
				assert.equal(res.value.length, 0);
				//selenium bug with drag and drop, to be investigated, this should be equal 1

			})
			.call(callback)
	});
};