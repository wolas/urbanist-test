Feature: Change guest status

  As a user
  I want to see an event
  So that I can change a guest status

  	@dev
	Scenario: User can change a guest status using drag and drop
		Given I will go on the home page
		And There is an event with one guest
		When I click on the event title
		Then I see event page
		When I drag a user to different status section
		Then A user will disappear from the previous section
		And the user will appear in new section