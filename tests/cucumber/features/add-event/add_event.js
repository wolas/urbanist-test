module.exports = function () {

	this.When(/^I am on the home page$/, function (callback) {
		this.client.url(process.env.ROOT_URL).call(callback);
	});

	this.When(/^I click on the add event button$/, function (callback) {
		this.client.
			waitForExist('.btn-add', 2000).
			waitForVisible('.btn-add').
			click('.btn-add').
			call(callback);
	});

	this.Then(/^I see event adding form$/, function (callback) {
		this.client
			.waitForExist('form.event-form', 2000)
			.call(callback)
	});

	this.When(/^I provide all necessary fields$/, function (callback) {
		this.client
			.waitForExist('input.event-add__name', 2000)
			.waitForExist('input.event-add__date', 2000)
			.setValue('input.event-add__name', 'Super New Tested Event')
      		.execute(function(){
      			$("input.event-add__date").val("2015-06-19T12:05");
      		})
      		.getAttribute("input.event-add__date", "value", function(err, res){
      			assert.equal(res, "2015-06-19T12:05");
      		})
			.call(callback)
	});

	this.When(/^I click submit button$/, function (callback) {
		this.client.submitForm('.event-form').call(callback)
	});


	this.Then(/^I will be redirected to the home page$/, function (callback) {
		this.client
			.pause(1000)
			.url(function(err, res){
				assert.equal(res.value, process.env.ROOT_URL)
			})
			.call(callback)
	});

	this.Then(/^I will see the event on the list$/, function (callback) {
		this.client
			.waitForExist('h4', 7000)
			.waitForVisible('h4', 2000)
			.getText('h4 a')
			.should.become("Super New Tested Event")
			.and.notify(callback);
	});

	this.When(/^I not provide all necessary fields$/, function (callback) {
		this.client
			.setValue('input.event-add__name', '')
			.call(callback);
	});

	this.Then(/^I will see an alert$/, function (callback) {
		this.client
			.waitForExist('.alert-danger', 7000)
			.waitForVisible('.alert-danger', 2000)
			.getText('.alert-danger', function(err, res){
				expect(res).to.include("Name cannot be empty")
			})
			.call(callback);
	});

};