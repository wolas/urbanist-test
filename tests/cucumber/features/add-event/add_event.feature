Feature: Add an event

  As a user
  I want to add an event
  So that I can manage the event

  	@dev
	Scenario: User can add an event when providig all necessary fields
		Given I am on the home page
		When I click on the add event button
		Then I see event adding form
		When I provide all necessary fields
		When I click submit button
		Then I will be redirected to the home page
		And I will see the event on the list

  	@dev
	Scenario: User cannot add an event when not providing all necessary fields
		Given I am on the home page
		When I click on the add event button
		Then I see event adding form
		When I not provide all necessary fields
		When I click submit button
		Then I will see an alert