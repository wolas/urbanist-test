Meteor.methods({
  'test/resetEvents': function () {
		EM.Events.remove();
  },
  'test/insertEvent': function (ev) {
  		check(ev, Object);
		EM.Events.insert(ev);
  }
});