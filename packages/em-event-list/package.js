Package.describe({
    summary: "Event list"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em', 'em-events'], both);
    api.use(['templating'], 'client');

    api.add_files('client/views/list.html', 'client');
    api.add_files('client/views/list.js', 'client');

    api.add_files('client/subscriptions/subscriptions.js', 'client');
});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers'], ['client']);
});