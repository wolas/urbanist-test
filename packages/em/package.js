Package.describe({
    summary: "Namespace package and configuration"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];

    api.add_files('common/model/model.js', both);
    api.add_files('client/model/model.js', 'client');

    api.export("EM");
});