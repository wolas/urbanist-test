Template.emAddGuestFormModal.events({
	'submit .addGuest-form': function(e, t) {
		var name = t.find(".guest-form__name").value,
			statusNode = t.find(".guest-form__status"),
			status = statusNode.options[statusNode.selectedIndex].value;

		Session.set("emLoading", true);
		EM.Events.addGuest.call(this, name, status, function(err, res){
			if(err) {
				Alerts.error(err.error, "addGuestForm");
			} else {
				$(t.find('#addGuestForm')).modal('hide');
			}
			Session.set("emLoading", false);
		});

		return false;
	}
});