Template.emGuest.events({
	'click .btn-remove': function(e, t) {
		var ev = Template.parentData(2);
		
		this.loading && this.loading.set(true);
		EM.Events.removeGuest.call(this, ev._id, this.name, function(err, res){
			if(err) {
				Alerts.error(err.error, "guestList");
			}
			this.loading && this.loading.set(false);
		});
	}
});