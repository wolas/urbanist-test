Template.registerHelper("getGuestsWithStatus", function(status){
	var ev = Template.parentData(1);
	if(!ev || !ev.guests) return [];
	
	return _.filter(ev.guests, function(guest){
		return guest.status === status;
	});
});