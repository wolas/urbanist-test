Template.registerHelper("getGuestsStatusCount", function(status){
	var ev = Template.parentData(1);
	if(!ev || !ev.guests) return 0;
	
	return _.filter(ev.guests, function(guest){
		return guest.status === status;
	}).length;
});