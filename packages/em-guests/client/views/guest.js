Template.emGuest.onCreated(function(){
	this.data.loading = new ReactiveVar(false);
});

Template.emGuest.helpers({
	getLoading: function() {
		return this.loading && this.loading.get();
	}
});