EM.Guests = (function(){
	var _guests = JSON.parse(Assets.getText("fixtures/guests.json"));

	var getMockGuests = function() {
		return _guests;
	};

	return {
		getMockGuests: getMockGuests
	};
})();