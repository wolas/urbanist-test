Package.describe({
    summary: "package"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em', 'reactive-var'], both);
    api.use(['templating', 'underscore'], 'client');

    api.add_files('fixtures/guests.json', both, {isAsset: true});

    api.add_files('client/views/helpers/getGuestsWithStatus.js', 'client');
    api.add_files('client/views/helpers/getGuestsStatusCount.js', 'client');

    api.add_files('server/model/model.js', 'server');

    api.add_files('client/views/guest.html', 'client');
    api.add_files('client/views/guest.js', 'client');
    api.add_files('client/views/events/guest.js', 'client');

    api.add_files('client/views/addForm.html', 'client');
    api.add_files('client/views/addForm.js', 'client');
    api.add_files('client/views/events/addForm.js', 'client');

});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers', 'em', 'ui', 'em-guests'], ['client', 'server']);

    api.add_files('server/tests/mocks.js', 'server');
    api.add_files('client/tests/helpers.js', 'client');
});