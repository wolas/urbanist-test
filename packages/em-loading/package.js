Package.describe({
    summary: "Loading spinner"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em'], both);
    api.use(['templating', 'sacha:spin@2.0.4', 'session'], 'client');

    api.add_files('client/views/loading.html', 'client');
    api.add_files('client/views/helpers/session.js', 'client');
});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers'], ['client']);
});