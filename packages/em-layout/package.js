Package.describe({
    summary: "Layout package"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];

    api.use(['em'], both);
    api.use(['templating', 'iron:router'], 'client');

    api.add_files('client/routes/router.js', 'client');
    api.add_files('client/views/layout.html', 'client');
});