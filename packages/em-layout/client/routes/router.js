Router.map(function(){
    this.route('home', function(){
        this.render();
    }, {
        path: '/',
        layoutTemplate: 'emLayout',
        template: EM.homeTpl,
        yieldTemplates: {
          'emHeader': {to: 'header'},
          'emFooter': {to: 'footer'}
        }
    });
});
