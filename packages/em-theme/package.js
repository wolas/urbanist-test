Package.describe({
    summary: "Theme package"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em', 'less'], both);

    api.add_files('client/views/styles/variables.less', 'client');
    api.add_files('client/views/styles/main.less', 'client');
    api.add_files('client/views/styles/eventList.less', 'client');
    api.add_files('client/views/styles/eventPage.less', 'client');

});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers'], ['client']);
});