var reset = function() {
	TestableEventsCollection.remove({});
};

var mockEvent = {
	name: "test",
	date: new Date(),
	guests: []
};

Tinytest.add(
	'events - inserting document',
	function (test) {
		reset();

		test.equal(TestableEventsCollection.find({}).fetch().length, 0);
		EM.Events.insert(mockEvent);
		test.equal(TestableEventsCollection.find({}).fetch().length, 1);
		EM.Events.insert(mockEvent);
		test.equal(TestableEventsCollection.find({}).fetch().length, 2);
	}
);

Tinytest.add(
	'events - removing document',
	function (test) {
		reset();

		test.equal(TestableEventsCollection.find({}).fetch().length, 0);
		var id = EM.Events.insert(mockEvent);
		test.equal(TestableEventsCollection.find({}).fetch().length, 1);
		EM.Events.remove(id);
		test.equal(TestableEventsCollection.find({}).fetch().length, 0);
	}
);

Tinytest.add(
	'events - getting all documents',
	function (test) {
		reset();

		test.equal(TestableEventsCollection.find({}).fetch().length, 0);
		EM.Events.insert(mockEvent);
		EM.Events.insert(mockEvent);
		EM.Events.insert(mockEvent);

		test.equal(TestableEventsCollection.find({}).fetch().length, 3);
		test.equal(EM.Events.getAll({}).fetch().length, 3);
	}
);

Tinytest.add(
	'events - getting one document',
	function (test) {
		reset();

		test.equal(TestableEventsCollection.find({}).fetch().length, 0);
		var id = EM.Events.insert(mockEvent);
		EM.Events.insert(mockEvent);
		EM.Events.insert(mockEvent);

		test.equal(TestableEventsCollection.find({}).fetch().length, 3);
		var doc = EM.Events.get(id);

		test.equal(doc.name, mockEvent.name);
		test.equal(doc.date, mockEvent.date);
		test.equal(doc.guests, mockEvent.guests);
	}
);

Tinytest.add(
	'events - updating documents',
	function (test) {
		reset();

		test.equal(TestableEventsCollection.find({}).fetch().length, 0);
		var id = EM.Events.insert(mockEvent);
		EM.Events.update({_id: id}, {name: "Hello"});

		test.equal(TestableEventsCollection.find({name: "Hello"}).fetch().length, 1);
	}
);

Tinytest.add('events - get guest guest - guest is not there', function(test){
	test.isFalse(EM.Events.getGuest.call("test", "test"));
});

Tinytest.add(
	'events - get guest guest - guest is there',
	function (test) {
		reset();

		test.equal(TestableEventsCollection.find({}).fetch().length, 0);
		var id = EM.Events.insert(mockEvent);
		EM.Events.update({_id: id}, {name: "Hello", guests: [{name: "Tested User"}]});

		test.isTrue(EM.Events.getGuest(id, "Tested User"));
	}
);