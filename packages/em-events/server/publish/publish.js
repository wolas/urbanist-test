Meteor.publish(EM.Events.pubName, function(selector){
	check(selector, Object);
	return EM.Events.getAll(selector);
});