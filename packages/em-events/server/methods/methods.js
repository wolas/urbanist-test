Meteor.methods({
	"events/remove": function(id) {
		check(id, String);
		return EM.Events.remove(id);
	},
	"events/add": function(name, date) {
		check(name, String);
		check(date, Date);
		check(name, Match.Where(EM.Validators.isNotEmptyString));
		check(name, Match.Where(EM.Validators.isStringNotTooShort));

		return EM.Events.insert({
			name: name,
			date: date,
			guests: EM.Guests.getMockGuests()
		});
	},
	"events/changeGuestStatus": function(event, guestName, status){
		check(event, String);
		check(guestName, String);
		check(status, String);
		
		return EM.Events.update({_id: event, "guests.name": guestName}, {$set: {
			"guests.$": {
				name: guestName,
				status: status
			}
		}});
	},
	"events/removeGuest": function(event, guestName) {
		check(event, String);
		check(guestName, String);

		return EM.Events.update({_id: event}, {$pull: {"guests": {"name": guestName}}});
	},
	"events/addGuest": function(event, guestName, status) {
		check(event, String);
		check(guestName, String);
		check(status, String);
		

		return EM.Events.update({_id: event}, {$push: {"guests": {"name": guestName, "status": status}}});
	}
});



