Package.describe({
    summary: "Events"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em', 'underscore', 'mongo', 'em-validators'], both);
    api.use(['templating', 'wolas:alerts', 'mrt:moment'], 'client');

    api.add_files('common/model/model.js', both);

    api.add_files('client/model/model.js', 'client');

    api.add_files('server/model/model.js', 'server');
    api.add_files('server/methods/methods.js', 'server');
    api.add_files('server/publish/publish.js', 'server');

    api.add_files('client/views/helpers/formatDate.js', 'client');

    api.add_files('client/views/eventPreview.html', 'client');
    api.add_files('client/views/eventPreview.js', 'client');
    api.add_files('client/views/events/eventPreview.js', 'client');

    api.export('TestableEventsCollection', ['client', 'server'], {testOnly: true});
});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers', 'wolas:alerts', 'em', 'em-events'], ['client', 'server']);

    api.add_files('server/tests/events.js', 'server');
    api.add_files('client/tests/methods.js', 'client');
    api.add_files('client/tests/events.js', 'client');
});