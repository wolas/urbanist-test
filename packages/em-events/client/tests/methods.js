testAsyncMulti("events - removing event method - id is not a string", [
	function(test, expect) {
		Meteor.call("events/remove", null, expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	},
	function(test, expect) {
		Meteor.call("events/remove", 5, expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	}
]);

testAsyncMulti("events - removing event method - id is a string", [
	function(test, expect) {
		Meteor.call("events/remove", "id", expect(function(err, res){
			test.isFalse(err);
			test.equal(res, 0); //no such id, so removed none
		}));
	}
]);

testAsyncMulti("events - adding an event method - name is not a string", [
	function(test, expect) {
		Meteor.call("events/add", null, new Date(), expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	},
	function(test, expect) {
		Meteor.call("events/add", 1, new Date(), expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	},
	function(test, expect) {
		Meteor.call("events/add", {}, new Date(), expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	}
]);

testAsyncMulti("events - adding an event method - name is an empty string", [
	function(test, expect) {
		Meteor.call("events/add", "", new Date(), expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	}
]);

testAsyncMulti("events - adding an event method - name is too short", [
	function(test, expect) {
		Meteor.call("events/add", "test", new Date(), expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	}
]);

testAsyncMulti("events - adding an event method - name is properly provided", [
	function(test, expect) {
		Meteor.call("events/add", "testtesttest", new Date(), expect(function(err, res){
			test.isFalse(err);
		}));
	}
]);

testAsyncMulti("events - adding an event method - date is not a Date object", [
	function(test, expect) {
		Meteor.call("events/add", "testtesttest", "", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	}
]);

testAsyncMulti("events - updating guest status method - event id is not a string", [
	function(test, expect) {
		Meteor.call("events/changeGuestStatus", null, "test", "test",expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	}
]);

testAsyncMulti("events - updating guest status method - guest name is not a string", [
	function(test, expect) {
		Meteor.call("events/changeGuestStatus", "test", null, "test",expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	}
]);

testAsyncMulti("events - updating guest status method - status is not a string", [
	function(test, expect) {
		Meteor.call("events/changeGuestStatus", "test", "test", null, expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
		}));
	}
]);

var id = null, id2 = null, id3 = null;

testAsyncMulti("events - updating guest status method - updating the status", [
	function(test, expect) {
		Meteor.call("events/add", "testtesttest", new Date(), expect(function(err, res){
			id = res;
		}));
	},
	function(test, expect){
		Meteor.call("events/changeGuestStatus", id, "Captain Crunch", "testStatus", expect(function(err, res){
			test.isFalse(err);
			test.equal(res, 1);
		}));
	}
]);

testAsyncMulti("events - removing guest method", [
	function(test, expect) {
		Meteor.call("events/add", "testtesttest", new Date(), expect(function(err, res){
			id2 = res;
		}));
	},
	function(test, expect){
		Meteor.call("events/removeGuest", id2, "Captain Crunch", expect(function(err, res){
			test.isFalse(err);
			test.equal(res, 1);
		}));
	}
]);


testAsyncMulti("events - adding guest method", [
	function(test, expect) {
		Meteor.call("events/add", "testtesttest", new Date(), expect(function(err, res){
			id3 = res;
		}));
	},
	function(test, expect){
		Meteor.call("events/addGuest", id3, "Tested User", "Tested status", expect(function(err, res){
			test.isFalse(err);
			test.equal(res, 1);
		}));
	}
]);
