var mockEvent = {
	_id: "test",
	name: "test",
	guests: []
};

testAsyncMulti("events - insert - name is empty string", [
	function(test, expect) {
		EM.Events.insert("", "Tue Jan 23 2015 22:03:23 GMT+0200 (CEST)", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - insert - name is not a string", [
	function(test, expect) {
		EM.Events.insert(null, "Tue Jan 23 2015 22:03:23 GMT+0200 (CEST)", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - insert - name is too short", [
	function(test, expect) {
		EM.Events.insert("test", "Tue Jan 23 2015 22:03:23 GMT+0200 (CEST)", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - insert - date is empty string", [
	function(test, expect) {
		EM.Events.insert("test test test test", "", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - insert - date is not a string", [
	function(test, expect) {
		EM.Events.insert("test test test test", null, expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - update guest status - event is not a string", [
	function(test, expect) {
		EM.Events.changeGuestStatus(null, "test", "test", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	},
	function(test, expect) {
		EM.Events.changeGuestStatus(1, "test", "test", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - update guest status - event is an empty string", [
	function(test, expect) {
		EM.Events.changeGuestStatus("", "test", "test", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);


testAsyncMulti("events - update guest status - guest is not a string", [
	function(test, expect) {
		EM.Events.changeGuestStatus("null", null, "test", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - update guest status - guest is an empty string", [
	function(test, expect) {
		EM.Events.changeGuestStatus("null", "", "test", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - update guest status - status is not a string", [
	function(test, expect) {
		EM.Events.changeGuestStatus("null", "test", null, expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - update guest status - status is an empty string", [
	function(test, expect) {
		EM.Events.changeGuestStatus("null", "test", "", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - add guest - status is not a string", [
	function(test, expect) {
		EM.Events.addGuest.call(mockEvent, "test", null, expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - add guest - status is an empty string", [
	function(test, expect) {
		EM.Events.addGuest.call(mockEvent, "test", "", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - add guest - name is not a string", [
	function(test, expect) {
		EM.Events.addGuest.call(mockEvent, 1, "test", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

testAsyncMulti("events - add guest - name is an empty string", [
	function(test, expect) {
		EM.Events.addGuest.call(mockEvent, "", "test", expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);

Tinytest.add('events - get guest guest - guest is not there', function(test){
	test.isFalse(EM.Events.getGuest.call("test", "test"));
});

testAsyncMulti("events - remove - id is not a string", [
	function(test, expect) {
		EM.Events.remove.call({_id: null}, expect(function(err, res){
			test.instanceOf(err, Meteor.Error);
			test.isNull(res);
		}));
	}
]);