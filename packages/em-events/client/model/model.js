_.extend(EM.Events, {
	insert: function(name, date, cb) {
		if(!cb) cb = function(){};

		if(!EM.Validators.isNotEmptyString(name)) {
			return cb.call(this, new Meteor.Error("Name cannot be empty"), null);
		}

		if(!EM.Validators.isStringNotTooShort(name)) {
			return cb.call(this, new Meteor.Error("Name is too short"), null);
		}

		if(!EM.Validators.isNotEmptyString(date)) {
			return cb.call(this, new Meteor.Error("Date cannot be empty"), null);
		}

		Meteor.call("events/add", name, new Date(date), function(err, res){
			cb.call(this, err, res);
		});

		return false;
	},
	remove: function(cb) {
		if(!cb) cb = function(){};

		if(!EM.Validators.isNotEmptyString(this._id)) {
			return cb.call(this, new Meteor.Error("Id cannot be empty"), null);
		}

		Meteor.call("events/remove", this._id, function(err, res){
			cb.call(this, err, res);
		});

		return false;
	},
	changeGuestStatus: function(eventId, name, status, cb) {
		if(!cb) cb = function(){};

		if(!EM.Validators.isNotEmptyString(eventId)) {
			return cb.call(this, new Meteor.Error("Event cannot be empty"), null);
		}

		if(!EM.Validators.isNotEmptyString(name)) {
			return cb.call(this, new Meteor.Error("Name cannot be empty"), null);
		}

		if(!EM.Validators.isNotEmptyString(status)) {
			return cb.call(this, new Meteor.Error("Status cannot be empty"), null);
		}

		Meteor.call("events/changeGuestStatus", eventId, name, status, function(err, res){
			cb.call(this, err, res);
		});

		return false;
	},
	removeGuest: function(eventId, name, cb) {
		var self = this;
		if(!cb) cb = function(){};

		if(!EM.Validators.isNotEmptyString(eventId)) {
			return cb.call(this, new Meteor.Error("Event cannot be empty"), null);
		}

		if(!EM.Validators.isNotEmptyString(name)) {
			return cb.call(this, new Meteor.Error("Name cannot be empty"), null);
		}
		
		Meteor.call("events/removeGuest", eventId, name, function(err, res){
			cb.call(this, err, res);
		});
	},
	addGuest: function(name, status, cb) {
		if(!cb) cb = function(){};

		if(!EM.Validators.isNotEmptyString(name)) {
			return cb.call(this, new Meteor.Error("Name cannot be empty"), null);
		}

		if(!EM.Validators.isNotEmptyString(status)) {
			return cb.call(this, new Meteor.Error("Status cannot be empty"), null);
		}

		if(EM.Events.getGuest(this._id, name)) {
			return cb.call(this, new Meteor.Error("This guest is already registered for the event"), null);
		}

		Meteor.call("events/addGuest", this._id, name, status, function(err, res){
			cb.call(this, err, res);
		});

		return false;
	},
});