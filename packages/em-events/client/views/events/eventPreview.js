Template.emEventPreview.events({
	'click .btn-remove': function(e, t) {
		EM.Events.remove.call(this, function(err, res){
			if(err) {
				Alerts.error(err.error, "eventList");
			}
		});
	}
});