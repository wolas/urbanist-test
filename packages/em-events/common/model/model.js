TestableEventsCollection = null;

EM.Events = (function(){
	var _collection = new Mongo.Collection("events");
	TestableEventsCollection = _collection;

	var pubName = "events";

	var getAll = function(sel) {
		return _collection.find(sel || {});
	};

	var get = function(id){
		return _collection.findOne({_id: id});
	};

	var remove = function(id) {
		var obj = {};
		if(id) obj._id = id;

		return _collection.remove(obj);
	};

	var insert = function(doc) {
		return _collection.insert(doc);
	};

	var update = function(sel, mod) {
		return _collection.update(sel, mod);
	};

	var getGuest = function(eventId, name) {
		var ev = _collection.findOne({_id: eventId, "guests.name": name}, {"guests.$": 1});
		return ev && ev.guests && ev.guests.length && ev.guests[0];
	};

	return {
		getAll: getAll,
		get: get,
		remove: remove,
		pubName: pubName,
		insert: insert,
		update: update,
		getGuest: getGuest
	};
})();

