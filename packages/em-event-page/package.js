Package.describe({
    summary: "Event page package"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em'], both);
    api.use(['templating', 'iron:router', 'em-guests', 'em-statuses'], 'client');

    api.add_files('client/routes/router.js', 'client');

    api.add_files('client/views/page.html', 'client');
    api.add_files('client/views/page.js', 'client');
    api.add_files('client/views/events/page.js', 'client');

});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers'], ['client']);
});