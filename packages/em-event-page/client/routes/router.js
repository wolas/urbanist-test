Router.map(function(){
    this.route('eventPage', function(){
        this.render();
    }, {
        data: function() {
            return EM.Events.get(this.params._id);
        },
        path: '/event/:_id',
        layoutTemplate: 'emLayout',
        template: 'emEventPage',
        yieldTemplates: {
            'emHeader': {to: 'header'},
            'emFooter': {to: 'footer'}
        },
    });
});