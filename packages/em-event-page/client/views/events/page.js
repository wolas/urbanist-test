Template.emEventPage.events({
	'dragstart .item': function(e, t) {
		e.originalEvent.dataTransfer.setData('text', this.name);
	},
	'dragover .event-page__status': function(e, t) {
		e.preventDefault();
	},
	'drop .event-page__status': function(e, t) {
		e.preventDefault();
		var eventId = Router.current().data()._id,
			name = e.originalEvent.dataTransfer.getData('text'),
			status = e.currentTarget.getAttribute("data-status");

		EM.Statuses.showLoading(status);
		EM.Events.changeGuestStatus(eventId, name, status, function(err, res){
			if(err) {
				Alerts.error(err.error, "guestList");
			}
			EM.Statuses.hideLoading(status);
		});
	}
});