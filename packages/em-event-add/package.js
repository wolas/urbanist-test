Package.describe({
    summary: "Event adding form"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em', 'reactive-var'], both);
    api.use(['iron:router', 'templating', 'wolas:alerts'], 'client');

    api.add_files('client/routes/router.js', 'client');

    api.add_files('client/views/add.html', 'client');
    api.add_files('client/views/add.js', 'client');
    api.add_files('client/views/events/add.js', 'client');

});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers'], ['client']);
});