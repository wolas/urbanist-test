Template.emEventAddPage.events({
	'submit .event-form': function(e, t) {
		var name = t.find(".event-add__name").value,
			date = t.find(".event-add__date").value;
		
		Session.set("emLoading", true);
		EM.Events.insert.call(this, name, date, function(err, res){
			if(err) {
				Alerts.error(err.error, "eventAddForm");
			} else {
				Router.go('home');
			}
			Session.set("emLoading", false);
		});

		return false;
	}
});