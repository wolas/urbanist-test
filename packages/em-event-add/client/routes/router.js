Router.map(function(){
    this.route('eventAddPage', function(){
        this.render();
    }, {
        path: '/events/add',
        layoutTemplate: 'emLayout',
        template: 'emEventAddPage',
        yieldTemplates: {
            'emHeader': {to: 'header'},
            'emFooter': {to: 'footer'}
        },
        onBeforeAction: function() {
            Alerts.clear("eventAddForm");
            this.next();
        }
    });
});