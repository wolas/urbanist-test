Tinytest.add('statuses - getting all statuses', function(test){
	test.equal(EM.Statuses.getAll().length, 3);
	test.equal(EM.Statuses.getAll()[0], "attending");
	test.equal(EM.Statuses.getAll()[1], "maybe");
	test.equal(EM.Statuses.getAll()[2], "not attending");
});