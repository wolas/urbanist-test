EM.Statuses = (function(){
	var _statuses = ["attending", "maybe", "not attending"];

	var loadings = {};

	_.each(_statuses, function(status){
		loadings[status] = new ReactiveVar(false);
	});

	var getAll = function() {
		return _statuses;
	};

	var showLoading = function(status) {
		return loadings[status] && loadings[status].set(true);
	};

	var hideLoading = function(status) {
		return loadings[status] && loadings[status].set(false);
	};

	var getLoading = function(status) {
		return loadings[status] && loadings[status].get();
	};

	return {
		getAll: getAll,
		showLoading: showLoading,
		hideLoading: hideLoading,
		getLoading: getLoading
	};
})();