Tinytest.add('statuses - helper - getting all statuses', function(test){
	test.equal(UI._globalHelpers.emStatuses().length, 3);
	test.equal(UI._globalHelpers.emStatuses()[0], "attending");
	test.equal(UI._globalHelpers.emStatuses()[1], "maybe");
	test.equal(UI._globalHelpers.emStatuses()[2], "not attending");
});
