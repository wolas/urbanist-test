Package.describe({
    summary: "Statuses package"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em', 'underscore', 'reactive-var'], both);
    api.use(['templating'], 'client');

    api.add_files('common/model/model.js', both);

    api.add_files('client/views/statuses.html', 'client');
    api.add_files('client/views/statuses.js', 'client');
    api.add_files('client/views/helpers/statuses.js', 'client');

});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers', 'em', 'em-statuses', 'ui'], ['client', 'server']);

    api.add_files('common/tests/statuses.js', ['client', 'server']);
    api.add_files('client/tests/helpers.js', 'client');
});