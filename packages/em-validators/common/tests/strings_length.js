Tinytest.add(
    'validators - string is not empty',
    function (test) {
        test.equal(EM.Validators.isNotEmptyString("short one"), true);
    }
);

Tinytest.add(
    'validators - string is empty',
    function (test) {
        test.equal(EM.Validators.isNotEmptyString(""), false);
    }
);

Tinytest.add(
    'validators - string too short',
    function (test) {
        test.equal(EM.Validators.isStringNotTooShort("short"), false);
        test.equal(EM.Validators.isStringNotTooShort("short", 6), false);
    }
);

Tinytest.add(
    'validators - string not too short',
    function (test) {
        test.equal(EM.Validators.isStringNotTooShort("not so short one"), true); // > 10
        test.equal(EM.Validators.isStringNotTooShort("short", 5), true);
    }
);