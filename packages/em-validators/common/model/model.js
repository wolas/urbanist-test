EM.Validators = {};

_.extend(EM.Validators, {
	isNotEmptyString: function(content){
		if(typeof content !== "string") {
			return false;
		}
		
		if(typeof content == "string" && content.length === 0){
			return false;
		}
		return true;
	},
	isStringNotTooShort: function(content, len){
		if(typeof content === "string" && content.length < (len || 10)){
			return false;
		}
		return true;
	}
});