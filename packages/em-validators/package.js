Package.describe({
    summary: "Common validators package"
});

Package.on_use(function (api) {
    var both = ['client', 'server'];
    api.use(['em', 'check', 'underscore'], both);

    api.add_files('common/model/model.js', both);
});

Package.on_test(function (api) {
    api.use(['tinytest', 'test-helpers', 'em', 'em-validators'], ['client', 'server']);

    api.add_files('common/tests/strings_length.js', ['client', 'server']);
});