#Solution; few comments

0. The application is package-oriented, since I believe this is the best way to write maintainable and scalable meteor application. The app is divided into seperated modules, with own responsibilites. The package is structured as "packages/sample-package", it may seem complicated at first, but once you get used to it, the package structure is obvious.

1. Current event publish has no limit, so it publishes all available events - I assume, that it is just a small demo, but normally I would consider some pagination and limits.

2. UI may be inconsistent in the app - eg. adding guest form and adding event form is different (separate route and bootstrap modal), but I did it on purpose to show different approaches. 

3. To change guest status, drag him to respective status section (no fallback for mobile devices).

4. For unit tests in packages I used tinytest, since it is the best option available for now. (mike:mocha-package is still very buggy)

5. For acceptance tests I used velocity + cucumber, you would have to run it locally to see results. 

# Urbanist candidate test #

your task is to create an event manager application

## an event has: ##
*  name
* date
*  guest list

## a guest has: ##
* name
* picture (doesn't have to be stored in db. you can use [http://lorempixel.com/50/50/people](http://lorempixel.com/50/50/people) to get randoms pictures)
* one of the following statuses: **attending**, **not attending**, or **maybe**

## the user can: ##
* add events
* select an event and view its guest list
* add/remove guests in the selected event
* change the status of each guest


## UI ##
* the guest list would be divided to separate sections for each status
* all of the actions in the application should update the UI reactively
* the application **must be responsive**

## NOTES ##
* the project already includes _nemo64:bootstrap_ and _less_ packages for bootstrap 3 styles
* if it's helpful, the project includes a fixture of guests at /fixtures/guests.json

## SUBMISSION ##
* to start the task, please fork this repository
* when you're done, deploy to a Meteor hosted site (*.meteor.com) and share the forked repository